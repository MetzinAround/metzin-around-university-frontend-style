---
title: Issues
nav_order: 3
---

## Week 1
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/1)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/2)

## Week 2
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/3)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/4)

## Week 3
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/5)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/6)

## Week 4
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/9)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/10)

## Week 5
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/11)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/12)

## Week 6
- [HW 1](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/7)
- [HW 2](https://gitlab.com/devops-education/workshops/examples/metzin-around-university-frontend-style/-/issues/8)