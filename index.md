---
title: Home
nav_order: 1
description: "Python 101"
permalink: /
---

# Python 101 with Professor Metz

Welcome to the Python 101. 

The syllabus is available [here](https://devops-education.gitlab.io//workshops/examples/metzin-around-university-frontend-style/course/syllabus/). 

The issues for homework are listed [here](https://devops-education.gitlab.io//workshops/examples/metzin-around-university-frontend-style/course/issues/)

