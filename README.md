# Static Site for Classroom use

This repo contains an example of the type of static site you can make using GitLab Pages. You're more than welcome to fork this to use for yourself, but make sure to change lines 9 and 10  in `_config.yml` to match the URL given to you in `deployments => Pages` after running the CI/CD pipeline for the first time. 

Some versions of GitLab have the pages settings under `settings => pages`
